# Работа с SELinux

# 1 часть - поднять NGinx на нестандартном порту.

Для начала устанавливаем пакет утилит для работы с SE

```yum install policycoreutils-python```

Мне известно три способа
1.  Узнаем, что не нравится SE через следующую команду

```
audit2why < /var/log/audit/audit.log
```
выдает рекомендацию, которая и является первым решением 
```
setsebool -P nis_enabled 1
```
Установка разрешения для `nis_enabled`
Полный список вкл/выкл булевых переключателей можно посмотреть командой `getsebool -a`

2. Так как мы знаем на какой порт мы перевели NGinx, можем сказать SE какой порт разрешить нужному сервису.

```
semanage port -a -t http_port_t -p tcp 5082
```
название сервиска как правило видно из ошибки показываемой в логах `/var/log/audit/audit.log`

3. анализ логов утилитой и создание модуля для SE
```
audit2allow -M httpd_add --debug < /var/log/audit/audit.log 
``` 
Утилита формирует модуль который и предлагает установить  `semodule -i httpd_add.pp`
Так же в папке лежат два файла `httpd_add.pp` и `httpd_add.te` - скомпилированная политика и человекочитаемая

# 2 часть - решить проблему на тестовом стенде.
Тестовый стенд ` https://github.com/mbfx/otus-linux-adm/blob/master/selinux_dns_problems/`
1. При запросе на обновление зоны с клиентской машины (Client) в аудит-логах сервера (NS01) обнаружились следующие записи 
`audit2allow -w -a`

Запрет на поиск в директории (как я это понял)
```
type=AVC msg=audit(1586892568.324:1958): avc:  denied  { search } for  pid=7333 comm="isc-worker0000"
name="net" dev="proc" ino=33530 scontext=system_u:system_r
:named_t:s0 tcontext=system_u:object_r:sysctl_net_t:s0 tclass=dir permissive=0
```
Запрет на созание файла журнала
```
type=AVC msg=audit(1586892905.522:2018): avc:  denied  { create } for  pid=7333 comm="isc-worker0000" 
name="named.ddns.lab.view1.jnl" scontext=system_u:system_r
:named_t:s0 tcontext=system_u:object_r:etc_t:s0 tclass=file permissive=0
```
Обе эти записи связаны с модулем named_t

2. Создадим модуль для SE c разрешениями для named_t 

```
audit2allow -M named_t --debug < /var/log/audit/audit.log
и сразу применим его 
semodule -i named_t.pp
```
3. Проверяем работу с клиента 
Появляется новая ошибка, при попытке записать в файл журнала

```
type=AVC msg=audit(1586896950.383:2030): avc:  denied  { write } for  pid=7333 comm="isc-worker0000" 
path="/etc/named/dynamic/named.ddns.lab.view1.jnl" dev="sda1"
ino=499656 scontext=system_u:system_r:named_t:s0 tcontext=system_u:object_r:etc_t:s0 tclass=file permissive=0
```
4. пересоздаем модуль для named_t
```
audit2allow -M named_t --debug < /var/log/audit/audit.log
и сразу применим его 
semodule -i named_t.pp
```
5. Перезапустим SE(я перезапустил целиком NS01) и попробуем заново обновить зону с клиента 
При попытке обновить зону, на клиенте снова возникает ошибка `update failed: SERVFAIL` , но на NS01 `tail -f /var/log/audit/audit.conf` не показывает никаких изменений,
следовательно с SE проблем нет. Анализ `/var/log/messages` выдает следующую ошибку:

ошибку доступа демона named к файлу содержащему записи о используемом диапазоне портов

```
SELinux is preventing /usr/sbin/named from open access on the file /proc/sys/net/ipv4/ip_local_port_range. 
For complete SELinux messages run: sealert -l 6bc4470a-dfb2-4686-a2f8-63cf4a5add0c
```
и тут же предлагает решить проблему 

```
ausearch -c 'isc-worker0000' --raw | audit2allow -M my-iscworker0000
применяем модуль
semodule -i my-iscworker0000.pp
```
6. Перезапустим на всякий случай named на NS01 `systemctl restart named`

7. Пробуем обновить опять и утыкаемся в ошибку. `Communication with 192.168.50.10#53 failed: timed out`
Очевидно firewalld на NS01

Узнаем какие порты использует днс в SE
```
semanage port -l |grep dns
```
Открываем 53 порт tcp/udp

```
firewall-cmd --zone=public --add-port=53/tcp --permanent
firewall-cmd --zone=public --add-port=53/udp--permanent
firewall-cmd --reload
```
8. Пробуем обновить опять и все получается. 

На NS01 в `/var/log/messages` 


```
Apr 14 21:17:40 ns01 named[2061]: client @0x7f0d080c15e0 192.168.50.15#45267/key zonetransfer.key: view view1: signer "zonetransfer.key" approved
Apr 14 21:17:40 ns01 named[2061]: client @0x7f0d080c15e0 192.168.50.15#45267/key zonetransfer.key: view view1: updating zone 'ddns.lab/IN': adding an RR at 'www.ddns.lab' A 192.168.50.15

```